.. console.py documentation master file, created by
   sphinx-quickstart on Fri May 11 10:50:40 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

console.py
======================================

Contents:

.. toctree::
   :maxdepth: 2

console.py is built out of the need for a better way to set up
read-process-print loops. The basic ``input()`` function in Python is
great for most things, but it doesn’t offer enough features for some
purposes and forces you to work around and write extra code to do really
basic things like providing a default value or testing for validity.

console.py solves these problems by providing the following features:

-  An optional introduction value
-  Custom prompt support
-  Tab completion (provided a list of completable verbs)
-  Default value
-  Ability to take in password input easily
-  Validity testing

Getting started with console.py
===============================
console.py requires the python modules ``getpass`` and ``readline``.  You may or may not need to install these before using console.py.

To start a new console, just do::

    c = Console(">>> ")

which sets up a new console with ">>> " as the default prompt (used when
nothing else is provided). From here, you can get input by using
something like:: 
    rawin = c.console()

Which will place the results of whatever the user inputs into the rawin
variable. The default prompt will be used once you have set it, but to
set a new prompt temporarily, just do::
    rawin = c.console(prompt="This is a new prompt: ")

Intro Messages
==============
You can set a custom intro message for one of your prompts by just doing::
	
	rawin = c.console(intro=“This is an intro”)

Which will output this is an intro on a separate line before the prompt.
Intros are temporary, you have to set one for each prompt.

Tab Completion
==============

Tab completion works by providing a list of strings that can be
completed by the ``readline`` module. For example, this list will allow the
user to use tab to autocomplete the values ``boat``, ``test``, and
``thespian``::

	rawin = c.console(autocomplete=[‘boat’, ‘test’, ‘thespian’])

Any of these terms anywhere in the string can be completed, and if terms
that are substrings of each other are given to autocomplete, the user
will be prompted to make a choice. For example, given the autocomplete
list of ``['test', 'testing', 'testitis']``, the user will get the
following on tab::

            >>> tes <TAB>
	    >>> test <TAB>
	    test testing testitis 
	    >>> test

Default Values
==============

Provide a default value to use by doing::

    rawin = c.console(default=“Value”, prompt=“Enter a value [Value]:”)

The user will **NOT** be notified automatically that there is a default
value available, you will still need to specify this in your prompt.
After the user enters a value, either that value will be returned (if it
was not ``None``) or the default value specified will be returned.

Passwords
=========

console.py uses ``getpass`` for getting password input from the command
line. To specify that an input is a password, just do::

	c.console(password=True)

Which changes the prompt to a password input.

Validity Testing
================

Validity testing requires that you pass a function to your ``Console``
object. The function should return either ``True`` on valid user input
or ``False`` on invalid user input. This function should receive one
argument. For example, to force the user to enter a number less than
100::

	def less_than_100(arg): 
            if arg < 100: 
                return True 
            else: 
                return False

	c.Console(valid=less_than_100)

The function will be called after user input and will do a simple loop
until the function returns ``True``.

..
    Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`

